#ifndef RANDOM_ACCESS_ITERATOR_HPP
# define RANDOM_ACCESS_ITERATOR_HPP

# include "utils.hpp"

namespace ft
{
	template<typename Iterator, typename Container>
	class random_access_iterator
	{
		public:
			typedef Iterator												iterator_type;
			typedef typename iterator_traits<Iterator>::iterator_category	iterator_category;
			typedef typename iterator_traits<Iterator>::value_type			value_type;
			typedef typename iterator_traits<Iterator>::pointer				pointer;
			typedef typename iterator_traits<Iterator>::reference			reference;
			typedef typename iterator_traits<Iterator>::difference_type		difference_type;

		protected:
			Iterator _ptr;

		public:
			random_access_iterator() : _ptr(iterator_type())
			{}

			explicit random_access_iterator(iterator_type const &it) : _ptr(it)
			{}

			template <typename Iter>
    		random_access_iterator(const random_access_iterator<
                    Iter, typename enable_if<is_same<Iter, typename Container::pointer>::value,
                                             Container>::type>& it)
        	: _ptr(it.base())
			{}

			~random_access_iterator() {}

			random_access_iterator & operator=(random_access_iterator const &y)
			{
				if (this != &y)
					_ptr = y._ptr;
				return *this;
			}

			const iterator_type&	base() const
			{
				return _ptr;
			}
			reference	operator*()
			{
				return *_ptr;
			}
			pointer		operator->()
			{
				return _ptr;
			}
			// prefix
			random_access_iterator	&operator++()
			{
				++_ptr;
				return *this;
			}
			// postfix
			random_access_iterator	operator++(int)
			{

				return random_access_iterator(_ptr++);
			}
			// prefix
			random_access_iterator	&operator--()
			{
				--_ptr;
				return *this;
			}
			// postfix
			random_access_iterator	operator--(int)
			{
				return random_access_iterator(_ptr--);
			}
			random_access_iterator operator+(difference_type n)
			{
				return random_access_iterator(_ptr + n);
			}
			random_access_iterator operator-(difference_type n)
			{
				return random_access_iterator(_ptr - n);
			}
			random_access_iterator& operator+=(difference_type n)
			{
				_ptr += n;
				return *this;
			}
			random_access_iterator& operator-=(difference_type n)
			{
				_ptr -= n;
				return *this;
			}
			reference	operator[](difference_type n)
			{
				return *(_ptr + n);
			}
	}; // end of random_access_iterator

	// iterator left, iterator right, container
	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator==(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() == y.base();
	}

	// ayni tur pointerlar icin
	template <typename Iterator, typename Container>
	inline bool operator==(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() == y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator!=(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() != y.base();
	}

	template <typename Iterator, typename Container>
	inline bool operator!=(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() != y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator<(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() < y.base();
	}

	template <typename Iterator, typename Container>
	inline bool operator<(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() < y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator<=(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() <= y.base();
	}

	template <typename Iterator, typename Container>
	inline bool operator<=(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() <= y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator>(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() > y.base();
	}

	template <typename Iterator, typename Container>
	inline bool operator>(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() > y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline bool operator>=(const random_access_iterator<IteratorL, Container>&x,
						   const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() >= y.base();
	}

	template <typename Iterator, typename Container>
	inline bool operator>=(const random_access_iterator<Iterator, Container>&x,
						   const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() >= y.base();
	}

	template <typename IteratorL, typename IteratorR, typename Container>
	inline typename random_access_iterator<IteratorL, Container>::difference_type
	operator-(const random_access_iterator<IteratorL, Container>&x,const random_access_iterator<IteratorR, Container>&y)
	{
		return x.base() - y.base();
	}

	template <typename Iterator, typename Container>
	inline typename random_access_iterator<Iterator, Container>::difference_type
	operator-(const random_access_iterator<Iterator, Container>&x, const random_access_iterator<Iterator, Container>&y)
	{
		return x.base() - y.base();
	}

	template <typename Iterator, typename Container>
	inline random_access_iterator<Iterator, Container>
	operator+(typename random_access_iterator<Iterator, Container>::difference_type n,
          const random_access_iterator<Iterator, Container>& it)
	{
    	return random_access_iterator<Iterator, Container>(it.base() + n);
	}
} // namespace ft

#endif
