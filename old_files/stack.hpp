/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:19:31 by akarahan          #+#    #+#             */
/*   Updated: 2022/08/09 18:30:02by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_HPP
# define STACK_HPP

# include <vector>
// # include "vector.hpp"

namespace ft
{
	template<typename T, typename Container = std::vector<T> >
	class stack
	{
		public:
			typedef Container						container_type;
			typedef typename Container::value_type	value_type;
			typedef typename Container::size_type	size_type;

		protected:
			container_type	_c;

		public:
			explicit stack(const container_type &c = container_type()) : _c(c) {}
			stack(const stack &copyObj) : _c(copyObj._c) {}

			stack	&operator= (const stack &copyObj)
			{
				if (this != &copyObj)
					_c = copyObj._c;
				return (*this);
			}

			~stack() {}

			bool	empty(void) const
			{
				return (_c.empty());
			}

			size_type	size(void) const
			{
				return (_c.size());
			}

			value_type	&top(void)
			{
				return (_c.back());
			}

			const value_type	&top(void) const
			{
				return (_c.back());
			}

			void	push(const value_type &val)
			{
				return (_c.push_back(val));
			}

			void	pop(void)
			{
				return (_c.pop_back());
			}

			template <class U, class Cont>
			friend bool operator== (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c == rhs._c);
			}

			template <class U, class Cont>
			friend bool operator!= (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c != rhs._c);
			}

			template <class U, class Cont>
			friend bool operator<  (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c < rhs._c);
			}

			template <class U, class Cont>
			friend bool operator<= (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c <= rhs._c);
			}

			template <class U, class Cont>
			friend bool operator>  (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c > rhs._c);
			}

			template <class U, class Cont>
			friend bool operator>= (const stack<U,Cont>& lhs, const stack<U,Cont>& rhs)
			{
				return (lhs._c >= rhs._c);
			}
	};
}

#endif
