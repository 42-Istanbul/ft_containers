/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/28 12:19:37 by akarahan          #+#    #+#             */
/*   Updated: 2022/08/10 20:53:38by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_HPP
# define VECTOR_HPP

# include <memory>
# include "./utils/utils.hpp"

namespace ft
{
	template<typename T, typename Alloc = std::allocator<T> >
	class vector
	{
		public:
			typedef	T											value_type;
			typedef Alloc										allocator_type;
			typedef typename allocator_type::reference			reference;
			typedef typename allocator_type::const_reference	const_reference;
			typedef typename allocator_type::pointer			pointer;
			typedef typename allocator_type::const_pointer		const_pointer;
			typedef ft::random_access_iterator<pointer, vector>			iterator;
			typedef ft::random_access_iterator<const_pointer, vector>	const_iterator;
			typedef typename allocator_type::size_type			size_type;
			typedef typename allocator_type::difference_type	difference_type;
			typedef ft::reverse_iterator<iterator>				reverse_iterator;
			typedef ft::reverse_iterator<const_iterator>		const_reverse_iterator;

		private:
			allocator_type	_alloc;
			pointer			_start;
			pointer			_end;
			pointer			_end_cap;

			void	checkRange(const size_type &n) const
			{
				if (n >= this->size())
					throw(std::out_of_range("vector::accessing out of bounds item"));
			}

			void construct_at_end(size_type n, const value_type& val)
				{
				while (n--)
					_alloc.construct(_end++, val);
			}

			void	destroy_until(pointer new_end)
			{
				while (_end != new_end)
					_alloc.destroy(--_end);
			}

		public:
			/*---------------Constructors---------------*/
			// Default constructor
			explicit vector(const allocator_type &alloc = allocator_type()) :
				_alloc(alloc),
				_start(NULL),
				_end(NULL),
				_end_cap(NULL)
			{}

			// Fill constructor
			explicit vector(size_type n, const value_type &val = value_type(), const allocator_type &alloc = allocator_type()) :
				_alloc(alloc),
				_start(NULL),
				_end(NULL),
				_end_cap(NULL)
			{
				_start = _alloc.allocate(n);
				_end_cap = _start + n;
				_end = _start;
				while (n--)
				{
					_alloc.construct(_end, val);
					++_end;
				}
			}

			// Range constructor
			template<typename InputIterator>
			vector(InputIterator first, InputIterator last, const allocator_type &alloc = allocator_type(),
				typename ft::enable_if<!ft::is_integral<InputIterator>::value>::type* = NULL) :
				_alloc(alloc),
				_start(NULL),
				_end(NULL),
				_end_cap(NULL)
			{
				for (; first != last; ++first)
					this->push_back(*first);
			}

			// Copy constructor
			vector(const vector &x) :
				_alloc(x._alloc),
				_start(NULL),
				_end(NULL),
				_end_cap(NULL)
			{
				*this = x;
			}

			/*---------------Destructor---------------*/
			~vector()
			{
				this->clear();
				_alloc.deallocate(_start, this->capacity());
			}

			/*---------------Copy assignment operator---------------*/
			vector &operator= (const vector &x)
			{
				if (this != &x)
					this->assign(x.begin(), x.end());
				return (*this);
			}

			/*---------------Iterators---------------*/
			iterator		begin()
			{
				return (iterator(_start));
			}

			const_iterator	begin() const
			{
				return (const_iterator(_start));
			}

			iterator		end()
			{
				return (iterator(_end));
			}

			const_iterator	end() const
			{
				return (const_iterator(_end));
			}

			reverse_iterator		rbegin()
			{
				return (reverse_iterator(this->end()));
			}

			const_reverse_iterator	rbegin() const
			{
				return (const_reverse_iterator(this->end()));
			}

			reverse_iterator		rend()
			{
				return (reverse_iterator(this->begin()));
			}

			const_reverse_iterator	rend() const
			{
				return (const_reverse_iterator(this->begin()));
			}

			/*---------------Capacity---------------*/
			size_type	size() const
			{
				return (_end - _start);
			}

			size_type	max_size() const
			{
				return (allocator_type().max_size());
			}

			void		resize(size_type n, value_type val = value_type())
			{
				const size_type	len = this->size();
				if (n > len)
					this->insert(this->end(), n - len, val);
				else if (n < len)
					this->destroy_until(_start + n);
			}

			size_type	capacity() const
			{
				return (static_cast<size_type>(std::distance(this->begin(), const_iterator(_end_cap))));
			}

			bool		empty() const
			{
				return (this->size() == 0 ? true : false);
			}

			void		reserve(size_type n)
			{
				if (n <= this->capacity())
					return ;
				pointer	new_start = _alloc.allocate(n);
				pointer	new_end = new_start + this->size();
				pointer	new_end_cap = new_start + n;

				try
				{
					std::uninitialized_copy(_start, _end, new_start);
				}
				catch (...)
				{
					_alloc.deallocate(new_start, n);
					throw ;
				}
				if (_start != NULL)
				{
					this->clear();
					_alloc.deallocate(_start, this->capacity());
				}
				_start = new_start;
				_end = new_end;
				_end_cap = new_end_cap;
			}

			/*---------------Element access---------------*/
			reference		operator[] (size_type n)
			{
				return (*(_start + n));
			}

			const_reference	operator[] (size_type n) const
			{
				return (*(_start + n));
			}

			reference		at(size_type n)
			{
				checkRange(n);
				return ((*this)[n]);
			}

			const_reference	at(size_type n) const
			{
				checkRange(n);
				return ((*this)[n]);
			}

			reference		front()
			{
				return (*(this->begin()));
			}

			const_reference	front() const
			{
				return (*(this->begin()));
			}

			reference		back()
			{
				return (*(this->end() - 1));
			}

			const_reference	back() const
			{
				return (*(this->end() - 1));
			}

			/*---------------Modifiers---------------*/
			// Assign vector content
			template<typename InputIterator>
			void	assign(InputIterator first, InputIterator last,
					typename ft::enable_if<!ft::is_integral<InputIterator>::value, InputIterator>::type* = NULL)
			{
				this->insert(this->begin(), first, last);
			}

			void	assign(size_type n, const value_type &val)
			{
				if (n > this->capacity())
				{
					vector tmp(n, val);
					tmp.swap(*this);
				}
				else if (n > this->size())
				{
					std::fill(this->begin(), this->end(), val);
					size_type extra = n - this->size();
					while (extra--)
					{
						_alloc.construct(_end, val);
						++_end;
					}
				}
				else
				{
					pointer it = std::fill_n(_start, n, val);
					this->destroy_until(it);
				}
			}

			// Add element at the end
			void	push_back(const value_type &val)
			{
				if (_end == _end_cap)
					this->reserve(this->capacity() ? 2 * this->capacity() : 1);
				_alloc.construct(_end, val);
				++_end;
			}

			// Delete last element
			void	pop_back()
			{
				_alloc.destroy(--_end);
			}

			// Insert elements
			iterator	insert(iterator position, const value_type &val)
			{
				const size_type idx = position - this->begin();
				insert(position, 1, val);
				return (iterator(_start + idx));
			}
			void		insert(iterator position, size_type n, const value_type &val)
			{
				if (n == 0)
					return ;

				difference_type d = position - this->begin();
				pointer p = _start + d;
				if (n <= static_cast<size_type>(_end_cap - _end))
				{
					pointer old_end = _end;
					if (n > static_cast<size_type>(_end - p))
					{
						size_type c = n - (_end - p);
						construct_at_end(c, val);
						n -= c;
					}
					if (n > 0)
					{
						std::copy_backward(p, old_end, old_end + n);
						std::fill_n(p, n, val);
					}
				}
				else
				{
					size_type new_cap = std::max(this->capacity() * 2, this->size() + n);
					pointer new_begin = _alloc.allocate(new_cap);
					pointer new_end = new_begin + this->size() + n;
					pointer new_end_cap = new_begin + new_cap;
					try
					{
						std::uninitialized_copy(_start, p, new_begin);
						for (size_type i = 0; i < n; ++i)
							_alloc.construct(new_begin + d + i, val);
						std::uninitialized_copy(p, _end, new_begin + d + n);
					}
					catch (...)
					{
						for (size_type i = 0; i < n; ++i)
							_alloc.destroy(new_begin + d + i);
						_alloc.deallocate(new_begin, n);
						throw;
					}
					if (_start != NULL)
					{
						this->clear();
						_alloc.deallocate(_start, this->capacity());
					}
					_start = new_begin;
					_end = new_end;
					_end_cap = new_end_cap;
				}
			}
			template<typename InputIterator>
			void		insert(iterator position, InputIterator first, InputIterator last,
						typename ft::enable_if<!ft::is_integral<InputIterator>::value, InputIterator>::type* = NULL)
			{
				if (first == last)
					return;

				difference_type d = position - this->begin();
				pointer p = _start + d;
				vector<T> tmp(first, last);
				size_type n = tmp.size();
				if (n <= static_cast<size_type>(_end_cap - _end))
				{
					std::copy_backward(p, _end, _end + n);
					std::copy(tmp.begin(), tmp.end(), p);
					_end += n;
				}
				else
				{
					size_type new_cap = std::max(this->capacity() * 2, this->size() + n);
					pointer new_begin = _alloc.allocate(new_cap);
					pointer new_end = new_begin + this->size() + n;
					pointer new_end_cap = new_begin + new_cap;
					try
					{
						std::uninitialized_copy(_start, p, new_begin);
						std::uninitialized_copy(tmp.begin(), tmp.end(), new_begin + d);
						std::uninitialized_copy(p, _end, new_begin + d + n);
					}
					catch (...)
					{
						_alloc.deallocate(new_begin, n);
						throw;
					}
					if (_start != NULL)
					{
						this->clear();
						_alloc.deallocate(_start, this->capacity());
					}
					_start = new_begin;
					_end = new_end;
					_end_cap = new_end_cap;
				}
			}

			// Erase elements
			iterator	erase(iterator position)
			{
				if (position + 1 != this->end())
					std::copy(position + 1, this->end(), position);
				--_end;
				_alloc.destroy(_end);
				return (position);
			}
			iterator	erase(iterator first, iterator last)
			{
				if (first != last)
				{
					if (last != this->end())
						std::copy(last, this->end(), first);
					pointer new_end = first.base() + (this->end() - last);
					destroy_until(new_end);
				}
				return (first);
			}

			// Swap content
			void	swap(vector &x)
			{
				std::swap(_start, x._start);
				std::swap(_end, x._end);
				std::swap(_end_cap, x._end_cap);
			}

			// Clear content
			void	clear()
			{
				destroy_until(_start);
			}

			/*---------------Allocator---------------*/
			// Returns a copy of the allocator object associated with the vector.
			allocator_type	get_allocator() const
			{
				return (_alloc);
			}
	};

	/*---------------Non-member function overloads---------------*/
	template <class T, class Alloc>
	bool operator== (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		if (lhs.size() != rhs.size())
			return false;
		return (ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class T, class Alloc>
	bool operator!= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(lhs == rhs));
	}

	template <class T, class Alloc>
	bool operator<  (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class T, class Alloc>
	bool operator<= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(rhs < lhs));
	}

	template <class T, class Alloc>
	bool operator>  (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (lhs > rhs);
	}

	template <class T, class Alloc>
	bool operator>= (const vector<T,Alloc>& lhs, const vector<T,Alloc>& rhs)
	{
		return (!(lhs < rhs));
	}

	template <class T, class Alloc>
	void	swap(vector<T,Alloc>& x, vector<T,Alloc> &y)
	{
		x.swap(y);
	}
}

#endif
